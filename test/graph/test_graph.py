#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: 
# @file: test_graph.py
# @time: 2018/7/27 17:14
# @Software: PyCharm

import unittest
from slapy.graph.util import DirectedEdge, Node, Graph, DirectedGraph


class Test_graph(unittest.TestCase):
    def test_graph_util_graph_1(self):
        pass

    def test_graph_util_graph_2(self):
        pass

    def test_graph_util_node_1(self):
        n1 = Node(1)
        n2 = Node('1')
        n3 = Node(n1)
        self.assertEqual(n1, n2)
        self.assertEqual(n3, n2)
    def test_graph_util_node_2(self):
        pass

    def test_graph_util_edge_1(self):
        pass

    def test_graph_util_edge_2(self):
        pass

    def test_graph_util_directedgraph_1(self):
        pass

    def test_graph_util_directedgraph_2(self):
        pass
