#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: __init__.py.py
# @time: 2018/9/23 22:59
# @Software: PyCharm


from slapy.swarm.woa.Whales import Whales
from slapy.swarm.woa.WOAEngine import WOAEngine
