# !/user/bin/env/python
# -*- coding: utf-8 -*-
# @Time    : 
# @Author  : 河北雪域网络科技有限公司 A.Star
# @Site    : www.snowland.ltd
# @file    : __init__.py.py
# @time    : 2018/4/9 11:14
# @Software: PyCharm

from slapy.swarm.cso.CSOEngine import CSOEngine
from slapy.swarm.cso.CSOParticle import CSOParticle
"""
竞争群算法
"""