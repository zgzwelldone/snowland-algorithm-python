#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: GM1_1_demo.py
# @time: 2018/9/4 10:54
# @Software: PyCharm


from slapy.prediction import GM_1_1_model
import numpy as np

# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.5


if __name__ == '__main__':
    f = lambda t, b: 0.3 * t ** 2 + b + 0.3 * np.random.randn(1, len(t))
    t = np.arange(0, 5, 0.2)
    b = 3
    n = 4
    x = f(t, b)
    gm = GM_1_1_model(x)
    g = gm.predict(n)
    gm.draw(n)
