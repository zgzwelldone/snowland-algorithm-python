#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 河北雪域网络科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.snowland.ltd
# @file: bas_demo.py
# @time: 2018/8/28 15:39
# @Software: PyCharm

from slapy.swarm.bas import BASEngine
import numpy as np

# 使用前请安装文件
# pip install snowland-algorithm
# 确保版本号大于0.0.4


def fun(vars):
    x, y = vars
    return np.sin(y) + np.cos(x)


if __name__ == '__main__':
    engine = BASEngine(fitness_function=fun)
    engine.run()
